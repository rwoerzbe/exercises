#!/bin/bash

mapfile -t campusids < "${1:-lab_sm_toolbox_checker_campusids.txt}"
gitlab_url="https://gitlab.nt.fh-koeln.de/gitlab"
app_name="toolbox"
gitlab_projects_api_url="${gitlab_url}/api/v4/projects"

get_from_url() {
    local url=$1
    curl -s -k -H "PRIVATE-TOKEN: ${GITLAB_PRIVATE_TOKEN}" "${url}"
}

projects_json=$(get_from_url "${gitlab_projects_api_url}?search=${app_name}&membership=true&per_page=50000")

get_id_for_campusId() {
    local url="${gitlab_url}/${1}/${app_name}"
    echo "${projects_json}" | jq '.[] | select(.web_url == "'"${url}"'") | .id'
}

delete_project_pipelines() {
    local project_id=$1
    local pipeline_ids=$(get_from_url "${gitlab_projects_api_url}/${project_id}/pipelines?per_page=50000&sort=asc" | jq '.[].id' | dos2unix)
    unset 'pipeline_ids[${#pipeline_ids[@]}-3]' # keep the last 3
    for pipeline_id in ${pipeline_ids}
    do
        echo "Deleting ${campusid}:${project_id}:${pipeline_id}"
        curl -s -k -H "PRIVATE-TOKEN: ${GITLAB_PRIVATE_TOKEN}" --request "DELETE" "${gitlab_projects_api_url}/${project_id}/pipelines/${pipeline_id}"
    done
}

for campusid in "${campusids[@]}"
do
    delete_project_pipelines $(get_id_for_campusId "${campusid}")
done


