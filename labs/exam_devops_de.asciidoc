== Prüfung in DevOps

Im Kurs DevOps werden relativ viele Themen aus Zeitgründen nur im Kern angeschnitten. Sämtliche Themen lassen sich in noch viel mehr Facetten beleuchten. Es ist das Ziel der Prüfung, dass Sie selbst eine solche zusätzliche "Facette" beisteuern.

Sie sollten dabei zu einem selbst gewählten, neuen DevOps-Thema ein kleines Tutorial erstellen. Dieses Tutorial stellt Ihr abzugebendes *Produkt* dar.

Zusätzlich sollen Sie über ein Video-Capturing beweisen, dass Ihr Tutorial auch wie beschrieben funktioniert. (In Windows 10/11 ist über Windows-Taste+G die XBox-Game-Bar erreichbar, die einen Screen-Recoder als Funktion hat.) Dieses Video stellt Ihre *Produktdokumentation* dar.

=== Beispielthemen
Mögliche Themen für Ihr Tutorial finden sich in verschiedenen Dimensionen: Sie können beispielsweise eine meiner Übungsaufgaben abwandeln, indem Sie eine andere Applikation inkl. eines anderen Tech-Stacks verwenden. Oder Sie bleiben bei HICCUP+Spring+Java+PostgreSQL, erweitern aber eins der Themen, beispielweise um Autoscaling oder Alerting. Nehmen Sie folgende Beispielthemen als Inspiration, die als Lernziel formuliert sind:

==== Google-Cloud-Cluster mit einer NextJS-basierten Applikation
Absolventen des Tutorials sollen eine NextJS-basierte Applikation in einem Google-Cloud-Cluster deployen können. Hierzu konfigurieren Sie entsprechende Resourcen in einem Google-Cloud-Projekt. Insbesondere verstehen sie die wesentlichen Inhalte des `startup-script.sh`, das beim Start von virtuellen Maschinen verwendet wird.

==== Überwachtes Autoscaling für HICCUP
Absolventen des Tutorials können eine Instanzgruppe von virtuellen Maschinen so konfigurieren, dass bei eine bestimmten Lastsituation neue Instanzen erzeugt oder auch gelöscht werden. Sie können besagte Lastsituationen als Schwellwerte auf Basis bestimmter Metriken (z.B. CPU-Utilization) definieren.

==== Erweitertes Monitoring und Alerting für HICCUP
Absolventen des Tutorials haben ein tiefergehendes Verständnis des Monitoring-Dashboards und wie sich welche Metriken bei eine bestimmten Last auf HICCUP ändern. Sie können die Last mittels eine JMeter-Testsuite simulieren. Zusätzlich können Sie Alert-E-Mails im Falle der Überschreitung bestimmter Schwellwerte automatisiert verschicken lassen.

==== Deployment von HICCUP in einem GKE-Cluster
Absolventen des Tutorials können HICCUP in einem gemangtem Kubernetes-Cluster (GKE) in der Google Cloud deployen und neue Versionen unterbrechungsfrei (per Rolling-Update) einspielen.

==== Deployment von HICCUP in Google Cloud Run
Absolventen des Tutorials können HICCUP mittels https://cloud.google.com/run[Google Cloud Run] deployen und updaten.

==== GitLab CICD mit Deployment in die Google Cloud für HICCUP
Absolventen des Tutorials können einen GitLab-CICD-Pipeline und ein Google Cloud Project derart konfigurieren, dass nach einem `git push` der aktuelle Source-Code-Stand von HICCUP auf einer GitLab-Instanz gebaut und automatisch in das Google Cloud Project deployt wird.

=== Bewertungskriterien
Die Prüfung wird anhand der folgenden Kritierien benotet:

==== Originalität und Herausforderung
Für die Note spielt eine Rolle, wie weit Sie Neuland betreten. Überspitzt formuliert wäre ein Tutorial à la "HICCUP wie gehabt in der Cloud deployen, aber als HICKUP geschrieben" nicht sehr originell. Ein Thema wie "Continuous Delivery von Machine-Learning-Modellen in der Google Cloud" haben wir beispielsweise gar nicht behandelt und wäre insoweit aus unserer Sicht originell und herausfordernd.

==== Umfang
Der Umfang, also die Quantität, spielt eine Rolle. Beispielsweise hat mein Tutorial zur Google-Cloud einen sehr großen Umfang. Schon die Hälfte davon wäre klar im Einserbereich bzgl. dieses Kriteriums.

==== Nachvollziehbarkeit
Wichtig für ein Tutorial ist vorallem die Nachvollziehbarkeit. Beschreiben Sie die auszuführenden Schritte möglichst detailliert und genau (auf Kommandozeilenebene). Gehen Sie bei Code-Snippets nicht davon aus, das der Kontext des Snippets unbedingt klar ist, also evtl. zu setzenden Umgebungsvariablen, umgebender Code mit evtl. Imports etc.
Also: Code & Context (pun intended).

==== Technische Korrektheit
Natürlich sollte alles, was Sie im Tutorial schreiben, auch technisch richtig sein. Fehler in zu kopierenden Kommandozeilen beispielsweise sollten vermieden werden.

==== Äußerliche Qualität
Hierunter fallen formale Kriterien wie Rechtschreibung, Interpunktion, Stil etc.; beim Video sind es die visuelle und akustische Qualität.

=== Abgabemodalitäten

==== Produktabgabe
Bitte pushen Sie in Ihr bereits vorhandenes Coding-Software-2-Repository. Ihr Produkt (Tutorial) soll eine einzige Markdown-Datei `devops_<Themenname>.md` oder AsciiDoc-Datei `devops_<Themenname>.asciidoc` sein, beispielsweise also `devops_autoscaling_hiccup.asciidoc`. Die AsciiDoc- oder Markdown-Datei muss fehlerfrei gerendert werden können.

Nehmen Sie folgendes Snippet (im AsciiDoc-Format) als Vorlage für die Datei:

----
= Autoscaling für HICCUP

== Formale Angaben

Ein Video zu folgendem Tutorial finden Sie unter https://blahblahblah.com/ycvhwekfjbo

Urheber dieses Tutorials ist: Vorname Nachname

== Lernziel

Absolventen dieses Tutorials können eine Instanzgruppe eines Google-Cloud-Projekt so konfigurieren, dass neue virtuelle Maschinen, auf denen die Applikation X betrieben wird, beim Erreichen bestimmter Schwellwerte erzeugt und beim Unterschreiten wieder gelöscht werden. Ferner wissen Sie, wie zur Demonstration des Autoscalings mittels JMeter eine geeignete Test-Suite konfiguriert wird.

== Schritt 1: Einrichtung eines Google-Cloud-Projekts
...
----

==== Produktdokumentation
Produzieren Sie ihr Video und laden es in einen beliebigen Datei-Share (Sciebo, DropBox, OneDrive, YouTube etc.) hoch. Fügen Sie die URL zu dem Video in Ihr Produkt (die Markdown/AsciiDoc-Datei ein) gemäß Vorlage oben ein.

Vergessen Sie nicht, die Datei erneut zu committen und zu pushen.