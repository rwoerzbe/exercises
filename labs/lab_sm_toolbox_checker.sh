#!/bin/bash

if [[ -z "${GITLAB_PRIVATE_TOKEN}" ]]
then
    echo "GITLAB_PRIVATE_TOKEN is not set. Please do
export GITLAB_PRIVATE_TOKEN=<your GitLab access token>"
    exit 1;
fi

mapfile -t campusids < "${1:-lab_sm_toolbox_checker_campusids.txt}"
output_file_name="$(basename $0 | cut -d. -f1).csv"
gitlab_url="https://gitlab.nt.fh-koeln.de/gitlab"
app_name="toolbox"
gitlab_projects_api_url="${gitlab_url}/api/v4/projects"
sep=";"

get_from_url() {
    local url=$1
    curl -m 60 -s -k -H "PRIVATE-TOKEN: ${GITLAB_PRIVATE_TOKEN}" "${url}"
}

projects_json=$(get_from_url "${gitlab_projects_api_url}?search=${app_name}&membership=true&per_page=50000")

get_id_for_campusId() {
    local url="${gitlab_url}/${1}/${app_name}"
    echo "${projects_json}" | jq '.[] | select(.web_url == "'"${url}"'") | .id'
}

get_status_code_from_url() {
    local url="$1"
    curl -m 60 -s -L -H "PRIVATE-TOKEN: ${GITLAB_PRIVATE_TOKEN}" -o /dev/null -w "%{http_code}" "${url}"
}

get_raw_file_contents_from_url() {
    local url="$1"
    local base64_content
    base64_content="$(get_from_url "${url}" | jq -r '.content')"
    if [[ "${base64_content}" != "null" ]]
    then
        echo "${base64_content}" | base64 -d
    else 
        echo ""
    fi    
}

get_raw_file_contents_from_path() {
    get_raw_file_contents_from_url "$(get_full_path $campusid $1)"
}

function url_encode() {
    echo "$1" \
    | sed \
        -e 's/%/%25/g' \
        -e 's/ /%20/g' \
        -e 's/!/%21/g' \
        -e 's/"/%22/g' \
        -e "s/'/%27/g" \
        -e 's/#/%23/g' \
        -e 's/(/%28/g' \
        -e 's/)/%29/g' \
        -e 's/+/%2B/g' \
        -e 's/,/%2C/g' \
        -e 's/-/%2D/g' \
        -e 's/:/%3A/g' \
        -e 's/;/%3B/g' \
        -e 's/?/%3F/g' \
        -e 's/@/%40/g' \
        -e 's/\$/%24/g' \
        -e 's/\&/%26/g' \
        -e 's/\*/%2A/g' \
        -e 's/\./%2E/g' \
        -e 's/\//%2F/g' \
        -e 's/\[/%5B/g' \
        -e 's/\\/%5C/g' \
        -e 's/\]/%5D/g' \
        -e 's/\^/%5E/g' \
        -e 's/_/%5F/g' \
        -e 's/`/%60/g' \
        -e 's/{/%7B/g' \
        -e 's/|/%7D/g' \
        -e 's/}/%7D/g' \
        -e 's/~/%7E/g'
}

get_full_path() {
    local campusid="$1"
    local urlencodedPath
    urlencodedPath=$(url_encode "$2")
    echo ${gitlab_projects_api_url}/"$(get_id_for_campusId $campusid)"/repository/files/"${urlencodedPath}"?ref=master
}

lines_of_repo_file() {
    local file_path="$1"
    local regex="$2"
    local file_contents="$(get_raw_file_contents_from_path "${file_path}")"
    echo "${file_contents}" | grep -P "${regex}" -c
}

get_status_code_for_repo_file() {
    local campusid="$1"
    local local_path="$2"
    get_status_code_from_url "$(get_full_path "${campusid}" "${local_path}")"
}

get_xpath_value_for_repo_file() {
    local file_path="$1"
    local xpath="$2"
    local file_contents="$(get_raw_file_contents_from_path "${file_path}")"
    if [[ "${file_contents}" != "" ]] 
    then 
        # install xmllint on Windows via
        # choco install xsltproc
        echo "${file_contents}" | sed 's/method name="<init>"/method name="\&lt;init\&gt;"/' | xmllint --xpath "${xpath}" -
    else
        echo ""
    fi
}

get_sha1_for_value() {
    local value="$1"
    echo "${value}" | openssl dgst -sha1 | awk '{print $2}'
}

get_sha1_for_repo_file() {
    local file_path="$1"
    local file_contents="$(get_raw_file_contents_from_path "${file_path}" | tr -d '\0')"
    if [[ "${file_contents}" != "" ]]    
    then 
        # install xmllint on Windows via
        # choco install xsltproc
        get_sha1_for_value "${file_contents}"
    else
        echo ""
    fi
}

get_hash_value_for_repo_file() {
    get_sha1_for_repo_file "$1" | cut -c -8
}

palindrome_service_test_coverage() {
    get_xpath_value_for_repo_file 'deliverables/jacoco.xml' "string(/report/package[@name='de/thk/rwoerzbe/toolbox/palindrome']/class[@name='de/thk/rwoerzbe/toolbox/palindrome/PalindromeService']/method[@name='isPalindrome']/counter[@type='BRANCH']/@missed)"
}

palindrome_controller_mockito_test_coverage() {
    get_xpath_value_for_repo_file 'deliverables/jacoco.xml' "string(/report/package[@name='de/thk/rwoerzbe/toolbox/palindrome']/class[@name='de/thk/rwoerzbe/toolbox/palindrome/PalindromeController']/method[@name='checkCandidate']/counter[@type='METHOD']/@covered)"
}

generic_statuscode_lines_hash_for_repo_file_headers() {
    local header_prefix="$1"
    local is_binary="$2"
    if [[ "${is_binary}" == "binary" ]]
    then
        echo "${header_prefix}_status_code${sep}${header_prefix}_hash_value"
    else
        echo "${header_prefix}_status_code${sep}${header_prefix}_lines${sep}${header_prefix}_hash_value"
    fi
}

generic_statuscode_lines_hash_for_repo_file_values() {
    local campusid="$1"
    local repo_file="$2"
    local is_binary="$3"
    local regex="$4"
    if [[ "${is_binary}" == "binary" ]]
    then
        echo "$(get_status_code_for_repo_file ${campusid} ${repo_file})${sep}$(get_hash_value_for_repo_file ${repo_file})"
    else 
        echo "$(get_status_code_for_repo_file ${campusid} ${repo_file})${sep}$(lines_of_repo_file ${repo_file} ${regex})${sep}$(get_hash_value_for_repo_file ${repo_file})"
    fi
}

changelog_issues() {
    local contents="$(get_raw_file_contents_from_path 'CHANGELOG.md')"
    local issues=""
    if [[ $(echo "${contents}" | grep -Pc '<<<<<<<') != 0 ]]
    then
        issues+="changelog_merge_conflict_markers "
    fi
    if [[ $(echo "${contents}" | grep -Pc '## \[.\..\..\]') -lt 2 ]]
    then
        issues+="changelog_missing_versions "
    fi
    echo "${issues}"
}

pom_issues() {
    local contents="$(get_raw_file_contents_from_path 'pom.xml')"
    local issues=""
    if [[ $(echo "${contents}" | grep -Pc 'TODO comment') != 0 ]]
    then
        issues+="pom_remaining_TODO_comments "
    fi
    if [[ $(echo "${contents}" | grep -Pc 'maven-javadoc-plugin') -lt 2 ]]
    then
        issues+="pom_missing_javadoc_plugin "
    fi
    if [[ $(echo "${contents}" | grep -Pc '<minimum>') -lt 1 ]]
    then
        issues+="pom_missing_code_coverage_enforcement "
    fi
    echo "${issues}"
}

solutions_issues() {
    local contents="$(get_raw_file_contents_from_path 'deliverables/solutions.asciidoc')"
    local issues=""
    if [[ $(echo "${contents}" | grep -Pc 'Clean up local branches') != 0 ]]
    then
        issues+="solutions_clean_up_local_branches_missing "
    fi
    if [[ $(echo "${contents}" | grep -Pc 'Effective POM') != 0 ]]
    then
        issues+="solutions_effective_pom_missing "
    fi
    if [[ $(echo "${contents}" | grep -Pc 'Build a Toolbox Standalone JAR') != 0 ]]
    then
        issues+="solutions_build_a_toolbox_standalone_jar_missing "
    fi
    echo "${issues}"
} 

vminstance_docker_hello_world() {
    gcloud compute ssh vminstance-01 --project toolboxdocker-${campusid}-01 --zone europe-west4-a --command "sudo docker run hello-world" | grep -c "Hello from Docker!"
}

gcr_toolbox_image() {
    gcloud container images describe gcr.io/toolboxdocker-${campusid}-01/toolbox:1.0.1 --project toolboxdocker-${campusid}-01 --format='value(image_summary.digest)' | tr -d "sha256:" | cut -c -8
}

toolbox_docker_status_code() {
    local external_ip=$(gcloud compute instances list --filter="name:vminstance-01" --project=toolboxdocker-${campusid}-01 --format="value(EXTERNAL_IP)")
    get_status_code_from_url http://${external_ip}:8080
}

toolbox_cicd_project_id() {
    get_from_url "${gitlab_projects_api_url}/$(get_id_for_campusId ${campusid})/variables/GCP_PROJECT_ID" | jq '.value' | tr -d '"'
}

toolbox_cicd_staging_url() {
    local project_id=$(toolbox_cicd_project_id)
    if [[ "${project_id}" != "null" ]]
    then
        gcloud run services describe toolbox-staging --project="${project_id}" --region=europe-west4 --platform=managed --format='value(status.url)'
    else
        echo ""
    fi
}

toolbox_cicd_staging_status_code() {
    get_status_code_from_url $(toolbox_cicd_staging_url)
}

toolbox_cicd_pipeline_status() {
    get_from_url "${gitlab_projects_api_url}/$(get_id_for_campusId ${campusid})/pipelines/latest" | jq '.status' | tr -d '"'
}

toolbox_cicd_pipeline_javadoc_artifact_size() {
    get_from_url ${gitlab_projects_api_url}/$(get_id_for_campusId ${campusid})/jobs | jq '[.[] | select(.name == "javadoc")][0].artifacts[] | select(.file_type == "archive").size'
}

# CVS Head line
echo \
campusid${sep}\
toolbox_gitlab_id${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "solutions")${sep}\
solutions_clean_up_local_branches${sep}\
solutions_effective_pom${sep}\
solutions_standalone_jar${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "clbasics")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "toolbox_mainpage" "binary")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "h2_console" "binary")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "changelog")${sep}\
changelog_issues${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "pom")${sep}\
pom_issues${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "pom_effective")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "palindrome_service")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "palindrome_service_test")${sep}\
palindrome_service_test_coverage${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "palindrome_controller_mockito_test")${sep}\
palindrome_controller_mockito_test_coverage${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "palindrome_selenium_test")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "k6_loadtest_js")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "k6_loadperformance")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "dockerfile_standalone")${sep}\
vminstance_docker_hello_world${sep}\
gcr_toolbox_image${sep}\
toolbox_docker_status_code${sep}\
toolbox_cicd_project_id${sep}\
toolbox_cicd_staging_url${sep}\
toolbox_cicd_staging_status_code${sep}\
$(generic_statuscode_lines_hash_for_repo_file_headers "gitlab-ci")${sep}\
toolbox_cicd_pipeline_status${sep}\
toolbox_cicd_pipeline_javadoc_artifact_size${sep}\
> ${output_file_name}

rm nohup.out

for campusid in "${campusids[@]}"
do
(nohup echo \
$campusid${sep}\
$(get_id_for_campusId ${campusid})${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} "deliverables/solutions.asciidoc")${sep}\
$(lines_of_repo_file "deliverables/solutions.asciidoc" "(?i)=== Clean up local branches")${sep}\
$(lines_of_repo_file "deliverables/solutions.asciidoc" "(?i)=== Effective POM")${sep}\
$(lines_of_repo_file "deliverables/solutions.asciidoc" "(?i)=== Build a Toolbox Standalone JAR")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} "deliverables/clbasics.sh" "text" "^(?!\s*$|\s*#.*).*$")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} "deliverables/toolbox-mainpage.png" "binary")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} "deliverables/h2-console-prio1-todos.png" "binary")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} "CHANGELOG.md")${sep}\
$(changelog_issues)${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} "pom.xml")${sep}\
$(pom_issues)${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} "deliverables/pom-effective.xml")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} "src/main/java/de/thk/rwoerzbe/toolbox/palindrome/service/PalindromeService.java")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} "src/test/java/de/thk/rwoerzbe/toolbox/palindrome/service/PalindromeServiceTest.java")${sep}\
$(palindrome_service_test_coverage)${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} "src/test/java/de/thk/rwoerzbe/toolbox/palindrome/controllers/PalindromeControllerMockitoTest.java")${sep}\
$(palindrome_controller_mockito_test_coverage)${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} "src/test/java/de/thk/rwoerzbe/toolbox/palindrome/PalindromeSeleniumTest.java")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} "src/test/k6/loadtests.js")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} "deliverables/load-performance.json")${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} "Dockerfile-standalone")${sep}\
$(vminstance_docker_hello_world)${sep}\
$(gcr_toolbox_image)${sep}\
$(toolbox_docker_status_code)${sep}\
$(toolbox_cicd_project_id)${sep}\
$(toolbox_cicd_staging_url)${sep}\
$(toolbox_cicd_staging_status_code)${sep}\
$(generic_statuscode_lines_hash_for_repo_file_values ${campusid} ".gitlab-ci.yml")${sep}\
$(toolbox_cicd_pipeline_status)${sep}\
$(toolbox_cicd_pipeline_javadoc_artifact_size)${sep}\

) &

done

wait

cat nohup.out >> ${output_file_name}