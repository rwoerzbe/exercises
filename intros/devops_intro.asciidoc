= DevOps

Dieser Abschnitt beinhaltet Informationen und Instruktionen zum Ablauf des Kurses *DevOps* im Studiengang Code & Context. Der Kurs ist so gestaltet, dass Studierende die zu vermittelnden Kompetenzen durch Selbststudium und begleitende Übungen selbst aneignen können. Persönliche Treffen -- auch in kleinen Gruppen -- sind nicht notwendig aber natürlich nicht verboten, sofern keine Corona-Auflagen dagegensprechen. Die Kommunikation kann allein über technische Hilfsmittel erfolgen.

== Organisatorisches

=== Kursunterlagen
Wichtige Kursunterlagen sind diese Datei, die insbesondere auch alle Übungen enthält sowie https://1drv.ms/u/s!AlPyUCZMuma7vN8340Xs7FgHDvlklg?e=4fGFzq[DevOps Resources], wo Slides und Videos zu finden sind.

Wenn Sie Fehler in der Aufgabenstellung korrigieren möchten oder Verbesserungs-/Erweiterungsvorschläge haben, führen Sie die Änderungen bitte direkt durch Clonen dieses Repos und Änderung auf einem separaten Branch mit Präfix `asgm-XX`, wobei das `XX` von Ihnen geeignet gewählt werden kann, z.B. `asgm-gcpfirewall` für eine Änderung in der Aufgabe zur GCP Firewall. Pushen Sie die Änderung und stellen Sie anschließend einen Merge-Request an mich (`rwoerzbe`).

=== Literatur
- Das frei verfügbare Ebook http://linuxcommand.org/tlcl.php[The Linux Command Line]
- Die https://docs.gitlab.com/ee/ci/[Online-Dokumentation zu CICD in GitLab]
- Die https://cloud.google.com/docs[Online-Dokumentation der Google Cloud]
- Die https://docs.docker.com/[Online-Dokumentation über Docker]
- Die https://docs.docker.com/[Online-Dokumentation über Kubernetes]

=== Einreichung von Lösungen
Bei bestimmten Aufgaben müssen Dateien mit Lösungen von Ihnen gepusht werden. Pushen Sie bitte in das Ihnen bislang zugewiesene Repository von Coding Software 2.

=== Chat
Es gibt einen https://chat.coco.study/students/channels/class-ss22-devops[Matter-Most-Channel `class-ss22-devops`].

=== Video-Konferenz
Es gibt eine https://th-koeln.zoom.us/j/88175926251?pwd=ZnkxVEpoR0ZxQzUyaHIvK0poQ2J2QT09[Zoom-Besprechung] (Passwort: `123456`), über die bei Bedarf Video-Konferenzen abgehalten werden. Bitte prüfen Sie vorab (mit Kommiliton*innen), ob Sie gut verständlich sind.

Über Zoom können Sie auch unkompliziert Ihren Bildschirm teilen, sofern Sie Unterstützung benötigen.

=== Ablauf

Jeder Tag steht unter einen bestimmten Thema, beispielsweise "Build-Automatisierung mit Maven", d.h. Sie lösen hierzu Übungsaufgaben und eigenen sich so überwiegend die Lerninhalte im Selbststudium an. Bitte pushen Sie Ihre Lösungen auf wie o.a.

[cols="1,3"]
.Tagesablauf (bis auf mittwochs (Abwesenheitstag))
|===
|Zeitraum      | TOP

|10:00 - 10:20 | Einstimmung, Rückfragen zur Übung des Tages in der https://th-koeln.zoom.us/j/88175926251?pwd=ZnkxVEpoR0ZxQzUyaHIvK0poQ2J2QT09[Zoom-Besprechung] (Passwort: `123456`) 
|10:20 - 13:00 | Selbststudium und Bearbeitung der Übungsaufgaben. Ich (René Wörzberger) stehe vor Ort in der Schanzenstraße und/oder per Mattermost und Zoom für Hilfestellungen bereit.
|13:00 - 13:20 | https://th-koeln.zoom.us/j/88175926251?pwd=ZnkxVEpoR0ZxQzUyaHIvK0poQ2J2QT09[Zoom-Besprechung] (Passwort: `123456`) mit Wrap-Up des Tagesfortschritts, Klärung wiederkehrender Fragen etc.
|13:20 - 15:00 | Selbststudium und Bearbeitung der Übungsaufgaben. Ich (René Wörzberger) stehe vor Ort in der Schanzenstraße und/oder per Mattermost und Zoom für Hilfestellungen bereit.
|15:00 - 10:00 | Impulsvortrag (Video) zum Thema des Folgetages gucken
|===

Ich (René Wörzberger) stehe von 10:00 bis 15:00 Uhr bei Bedarf bereit, um Sie bei Verständnisproblemen (und natürlich auch unverständlichen / falschen Aufgabenstellungen) zu unterstützen. In der Regel lasse ich die https://th-koeln.zoom.us/j/88175926251?pwd=ZnkxVEpoR0ZxQzUyaHIvK0poQ2J2QT09[Zoom-Besprechung] (Passwort: `123456`) durchgängig laufen und bin darüber ansprechbar. Vorbehaltlich einer noch zu führenden Diskussion bin ich auch in der Schanzenstraße vor Ort.

Falls ich oder Sie nicht in der Schanzenstraße sind, kontaktieren Sie mich am besten per https://chat.coco.study/students/messages/@rwoerzbe[Direct-Message über Mattermost] oder über den https://chat.coco.study/students/channels/class-ss22-devops[Matter-Most-Channel `class-ss22-devops`] Dabei `@rwoerzbe` bitte als https://docs.mattermost.com/help/messaging/mentioning-teammates.html[Mention] unterbringen, um auf meiner Seite eine Push-Notification zu erzeugen. Wenn es auch für andere Teilnehmer dringlich ist, bitte `@channel` als Mention angeben.

Im Folgenden wird tageweise der Ablauf des Kurses erläutert. Wenn Sie ein Thema für Ihr bevorzugtes <<Exam, Prüfungstutorial>> relevant ist, das erst spät im Kurs besprochen wird, können Sie das betreffende Thema auch gerne mit dem aktuellen Tagesthema tauschen und die entsprechende Übung vorziehen. Ich versuche, Sie aus der Reihe auch dabei zu unterstützen.

==== Woche 1 - Montag: Linux und Command-Line-Basics
===== Inhalt
Ein Grundverständnis, was ein Betriebssystem macht und wie grundlegende Kommandozeilenbefehle funktionieren, ist trotz aller DevOps-Tools nach wie vor wichtig. Daher ist der erste Tag diesen Themen gewidmet. 

===== Aufgaben
- Schauen Sie zur Einstimmung die Aufzeichnung des GOTO-Conference-Talks https://www.youtube.com/watch?v=qmh7Uppd3x0[Mastering the Linux Command Line]
- Lesen Sie den Artikel https://medium.com/cracking-the-data-science-interview/how-operating-systems-work-10-concepts-you-should-know-as-a-developer-8d63bb38331f[The 10 Operating System Concepts Software Developers Need to Remember], um die wichtigsten Mechanismen und Begriffe des Linux-Betriebssystems zu verstehen.
- Lösen Sie die Aufgaben in <<Setup>> und <<Basics der Linux-Kommandozeile>> unter Zuhilfenahme von http://linuxcommand.org/tlcl.php[The Linux Command Line]

==== Woche 1 - Dienstag: Git Revisited
===== Inhalt
Sie haben in den vergangenen Kursen bereits den grundlegenden Umgang mit Git en passant gelernt. An diesem Tag wollen wir uns die wesentlichen Befehle, aber auch die Mechanismen "unter der Haube" von Git näher anschauen. Ein Lernziel ist insbesondere, Git als alltägliches und unumgängliches Entwicklungswerkzeug zu entmystifizieren.

===== Aufgaben
- Schauen Sie sich das oder die https://1drv.ms/u/s!AlPyUCZMuma7vN9hvswJtRJa_CjJmQ?e=Uw3hyt[Videos] zum heutigen Thema an.
- Lösen Sie die Aufgaben in <<Git>>

==== Woche 1 - Mittwoch: Day Off
An diesem Tag findet kein zusätzliches Programm statt. Sie haben die Möglichkeit, anderen Verpflichtungen nachzukommen, das bisher Gelernte zu reflektieren oder bisher ungelöste Aufgaben nachzuarbeiten. Machen Sie sich bitte auch frühzeitig über Ihr <<Prüfung in DevOps,Tutorial als Prüfungsthema>> Gedanken und formulieren Sie ggf. bereits das Lernziel.

Ich, René Wörzberger, stehe remote für Fragen zur Verfügung insbesondere bzgl. des besagten Lernziels Ihres Tutorials.

==== Woche 1 - Donnerstag: Build-Automatisierung mit Maven
===== Inhalt
Auch die Build-Automatisierung mit Maven haben Sie in vergangenen Kursen nur am Rande kennengelernt. Lernziel dieses Tages ist, das in der Java-Welt verbreitete Build-Tool Maven genauer zu verstehen. Sie sollen dabei insbesondere befähigt werden, die Konfigurationsdatei Ihrer eigenen HICCUP-Version zu verändern.

===== Aufgaben
- Schauen Sie sich das oder die https://1drv.ms/u/s!AlPyUCZMuma7vN9hvswJtRJa_CjJmQ?e=Uw3hyt[Videos] zum heutigen Thema an.
- Lösen Sie die Aufgaben in <<Build-Automatisierung mit Maven>>.

==== Woche 1 - Freitag: Testautomatisierung und Code-Coverage
===== Inhalt
Bestandteil eines automatisierten Builds sind i. d. R. auch die Ausführung von Unit-Tests. Wir wollen an diesem Tag wiederholen, wie Unit-Tests funktionieren. Die Güte von Test wird dabei nicht nur daran gemessen, ob sie "grün" werden, also das erwartete Ergebnis liefern. Zusätzlich ist die sogenannte Code-Coverage wichtig, die besagt, wie viel Code (automatisiert) getestet wird. Wie diese gemessen wird und wie die Messung in einen Maven-Build eingebaut wird, ist Thema dieses Tages.

===== Aufgaben
- Schauen Sie sich das oder die https://1drv.ms/u/s!AlPyUCZMuma7vN9hvswJtRJa_CjJmQ?e=Uw3hyt[Videos] zum heutigen Thema an.
- Lösen Sie die Aufgaben in <<Qualitätssicherung>>.

==== Woche 2 - Montag: Cloud Computing mit Google Cloud
===== Inhalt
Die erste Woche stand im Zeichen von "Dev". Wir haben alle Aufgaben jeweils auf unseren lokalen Rechnern erledigt und uns vornehmlich mit dem strukturierten, qualitätsgesicherten und wiederholbaren Bau unserer Applikation beschäftigt. 

Die zweite Woche wird im Zeichen von "Ops": Eine produktive Applikation läuft natürlich auf Dauer auf einem lokalen Rechner sondern heutzutage in einer Cloud-Umgebung. Ziel für diesen Tag ist, die Applikation HICCUP so zu in der Google Cloud installieren, das man sie produktiv verwenden könnte, insbesondere mit sicherer Kommunikation und skaliert für potentiell viele Clients.

===== Aufgaben
- Schauen Sie sich das oder die https://1drv.ms/u/s!AlPyUCZMuma7vN9hvswJtRJa_CjJmQ?e=Uw3hyt[Videos] zum heutigen Thema an.
- Lösen Sie die Aufgaben in <<Google Cloud Platform>> 

==== Woche 2 - Dienstag: Container Virtualisierung und Orchestrierung mit Docker und Kubernetes
===== Inhalt
Wir haben bislang in der Google Cloud unsere Applikation direkt auf virtuellen Maschinen laufen lassen. Mit sogenannten Containern hat sich allerdings in den letzten Jahren ein "leichtgewichtigeres" Virtualisierungsverfahren durchgesetzt, das erstens die Ressourcen in der Cloud besser nutzt und zweitens sämtliche lokalen Abhängigkeiten einer Applikation (beispielsweise die Java Runtime Environment) ein einem einzigen, sogenannten Image zusammenfassen kann, was eine Applikation(sversion) viel portabler (zwischen Entwicklungsrechner und produktiver Cloud) macht.

Lernziel für den heutigen Tag ist, die grundsätzlichen Mechanismen von Container-Virtualisierung mit Docker und Container-Orchestrierung mit Kubernetes zu verstehen und auch anwenden zu können.

===== Aufgaben 
- Schauen Sie sich das oder die https://1drv.ms/u/s!AlPyUCZMuma7vN9hvswJtRJa_CjJmQ?e=Uw3hyt[Videos] zum heutigen Thema an.
- Lösen Sie die Aufgaben in <<Container Virtualization with Docker>>

==== Woche 2 - Mittwoch: Day Off
s. <<Woche 1 - Mittwoch: Day Off>>

==== Woche 2 - Donnerstag: Continuous Integration - Continuous Delivery (CICD) mit GitLab
===== Inhalt
Abschließend bauen wir mit CICD eine Brücke zwischen Dev und Ops. CICD hat zum Ziel, Source-Code stets in einem releasefähigen Zustand zu halten und unterstützt durch die Konsequente Build-, Test- und Deploymentautomatisierung ein häufiges Deployment.

Lernziel dieses Tags ist, CICD in Sinne von GitLab zu verstehen und anwenden zu können. Idealerweise wird Ihre HICCUP-Version nach einer Source-Code-Änderung und einem `git push` vollautomatisch in ein Google-Cloud-Projekt deployt.

===== Aufgaben
- Schauen Sie sich das oder die https://1drv.ms/u/s!AlPyUCZMuma7vN9hvswJtRJa_CjJmQ?e=Uw3hyt[Videos] zum heutigen Thema an.
- Lösen Sie die Aufgaben in <<Continuous-Integration mit GitLab>>

==== Woche 2 - Freitagvormittag: Retrospektive und Zeit für Dokumentation
Am letzten Freitag wird kein weiterer Stoff vermittelt. Stattdessen nutzen wir die Zeit zur Behebung verbleibender (technischer) Probleme und für Rückfragen



