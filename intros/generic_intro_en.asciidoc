== Common Remarks
Most of the tasks in <<sec:exercises,Exercises>> are thightly guided tutorials. If possible, work on the tasks alone on your own machine. Your laptop should be 100% charged before the lecture / exercise. 

=== Questions and Answers

Some exercises include test questions. The answers are in a collapsible paragraph. Please try to answer the questions by yourself before reviewing the answer.

=== Line Breaks in Command Lines

Please note that some single command lines include line breaks which have to be escaped with a `\`. 

Example:

 echo "Ich bin ein ziemlich langer Parameter, der aber in einer einzigen \
 Kommandozeile verwendet wird, die daher mittels Backslash \
 umgebrochen wird und das sogar über mehrere Zeilen."

== Software Prerequisites

=== Bash

All commands in the exercises in labs are supposed to be executed in a https://www.gnu.org/software/bash/[Bourne Again Shell (Bash)] command line interpreter. Linux distributions are shipped with a Bash, MacOS uses the mostly compatible Z Shell (zsh). If you install <<Git>> on a Windows system, you get the GitBash.

Please refrain from executing the commands in the classic Windows command line interpreter (cmd.exe) or PowerShell.

=== Git

Install https://git-scm.com/[the Git command line client]. Keep the default settings during the installation. After the installation, windows users are now supposed to have the `Git Bash` in their start menu, which emulates Linux' Bash. MacOS should also have something like a Bash (ZSH works as well). 

In your Bash, execute

 git --version

which should something like

 git version 2.42.0.windows.1

Set your username and email address globally in Git.

CAUTION: As your `user.name` you MUST use your Firstname and Lastname in that order and captialization. For `user.email`, you *MUST* use your smail address.

 git config --global user.name "Firstname Lastname"
 git config --global user.email "firstname.lastname@smail.th-koeln.de"

If you cannot set these properties globally (because you already work with Git in another context than this course), set them locally like above in each Git repository we clone or init in the context of this course. In this case, you have just to leave out the `--global` command line option.

Check via 

 git config --list

if these properties have been set properly.

=== GitLab 

==== Register

Open {gitlaburl} in your browser. Please note that you have to be connected to the VPN in order to do that.

You *MUST* register with the following properties

. *Username*: your lowercase campus id which is like `flastna3` (= your Ilu login; *NOT* your matriculation number) 
. *Email*: your smail email address like `firstname.lastname@smail.th-koeln.de`. This must be exacly the same like <<Git,in `user.email`>>; otherwise, your commits won't really count as yours.

image:image1.png[image,width=199,height=283]

==== Create SSH Keys

In a Bash, check via

 ls -l ~/.ssh

if you already got a key pair. If so, the output should include something like

 ~/.ssh/id_rsa
 ~/.ssh/id_rsa.pub

Otherwise, execute

 ssh-keygen

which creates the abovementioned files. The passphrase can be left empty.

The key pair `id_rsa` (private key) and `id_rsa.pub` (public key) are now used to authenticate your local user at a SSH server. In our case, the GitLab server is such a SSH server if we use the `ssh://` scheme.

WARNING: The private key `id_rsa` MUST NOT be made public in any case. It should never be sent via email or copied to shared storage etc. Otherwise, it should be considered compromised and unsecure.

==== Paste Public Key into GitLab Settings

In GitLab, open `User Profile (icon) / Preferences / SSH Keys`, create a new key with `Add new key` and paste the contents of your local `~/.ssh/id_rsa.pub`. The `Expiration date` can be left indefinite.

The contents of your local public key can be displayed via

 cat ~/.ssh/id_rsa.pub

for copy to clipboard. After `Add key` your local user can authenticate to the GitLab server with that public key.

==== Add gitlab to `known_hosts`

Conversely, in order to let your local user trust the GitLab server SSH credentials (i.e. the SSH server must also authenticate to your local user), add its hostname like so:

[subs="attributes"]
----
ssh-keygen -R [gitlab.nt.fh-koeln.de]:10022
ssh-keygen -R [git.coco.study]:22
ssh-keyscan -t rsa -p 10022 gitlab.nt.fh-koeln.de >> ~/.ssh/known_hosts
ssh-keyscan -t rsa -p 22 git.coco.study >> ~/.ssh/known_hosts
----

=== Java 

Install the Java JDK in version 21. Please ensure, that the executables `java` and `javac` are in the search `PATH` for executables and the `JAVA_HOME` environment variable is set to the installation directory of the JDK. So, in a Git Bash that has been started after the installation of Java the following should output something else than an error messages:

 java -version
 javac -version
 "$JAVA_HOME/bin/java" -version

=== Maven

Download and install https://maven.apache.org/download.cgi[Apache Maven]. Again please ensure that the executable `mvn` is in the search `PATH`. So, in a Git Bash the following should output something else then nothing or error messages:

 mvn -version

=== IDE

As IDEs / source code editors, we mainy support https://www.jetbrains.com/idea/[IntelliJ IDEA] and https://code.visualstudio.com/[Visual Studio Code]. (Currently, the exercises are adapted for Visual Studio code but should still work with IntelliJ IDEA).

Students can obtain free licenses for IntelliJ here: https://www.jetbrains.com/shop/eform/students

CAUTION: Please carefully read the license agreement. Accepting these agreements and possible violations are completely in your responsibility.

== Google Cloud

We will make intensive use of the Google Cloud during the course. Some preparational steps are necessary there, too.

=== Google Account

You will need a Google Account identified via an email like `firstname.lastname@gmail.com` to login into the Google Cloud. Feel free to use your existing Google Account or to create a (separate and pseudonymous) one via https://accounts.google.com/[Google Accounts].

=== Cloud Shell

With your Google Account, you can login into the https://cloud.google.com[Google Cloud].

There are several ways for managing resources in the Google Cloud. We mainly use the command line tool https://cloud.google.com/sdk/gcloud[`gcloud`] in the web terminal https://cloud.google.com/shell[Cloud Shell] instead of the Web UI of the https://console.cloud.google.com/[Cloud Console]. The Cloud Shell is a virtual machine that is not billed and independend of any Google Cloud project with preinstalled command line tools like `gcloud`, `docker` and `kubectl` which is used by means of a terminal that runs in the browser.

In order to open the Cloud Shell, log into https://console.cloud.google.com/[Cloud Console]. Then, open a Cloud Shell using the button for `Activate Cloud Shell` on the top right corner. After a while, a terminal should pop up at the bottom of the browser tab. Please check, if you can invoke `gcloud`

 gcloud --version

IMPORTANT: Unless otherwise stated, all the commands in this exercise have to be executed in the Cloud Shell. Please note that the Cloud Shell disconnects after some time of inactivity and requires to have the region and zone to be set again (cf. <<gcloud Configuration>>)

We will use the Cloud Shell every now an then to connect to virtual machines via SSH. You should prepare SSH by creating a keypair in directory `.ssh` via

 gcloud compute config-ssh

CAUTION: Leave the `passphrase` empty, i.e., just press Return.

[INFO]
====
* You could also install `gcloud` locally in your machine and use it there instead of in the Cloud Shell. For sake of comparability, we all use the Cloud Shell in the exercises.
* Pasting text sometimes works with `Shift+Insert` and sometime with `Ctrl+V` depending on the client's OS and browser.
* APIs in the Google Cloud must be activated per project at first use. In such cases please just follow the URL in the terminal output and activate the API.
* Most of the following steps can also be done via the web UI of the Cloud Console. However, it is up to you to transfer the Cloud Shell commands into actions within the web UI.
====

=== Billing

Your usage of Google Cloud in this course is financed by free coupons for educational purposes. Be aware that Google also offers free 300$ trials for everybody, which ask for payment information during registration.

WARNING: #[.big]##*Please do not provide any payment information in the context of this course! Also not that in addition to the https://cloud.google.com/terms/[Google Cloud Terms of Services] and https://cloud.google.com/terms/aup[Acceptable Use Policy] the https://cloud.google.com/terms/free-trial[Free Trial Terms] also apply that rule out crypto mining etc.*###

In order to obtain a coupon and charge your Google Cloud billing account with it, follow the following steps:

. Open the respective coupon retrival page. The actual URL is specific to your course an will be revealed in the respective course.
. Enter your data. Enter your @smail.th-koeln.de email address as only these email addresses are eligible for receiving coupons.
. Poll your @smail emails
. Verify your @smail email address
. Again, poll your @smail emails
. Copy the coupon code
. (If you have no Google account yet, please create one and log in.)
. Call https://console.cloud.google.com/education and paste the coupon code there.

Open the billing in the Cloud Console. There is supposed to be a billing account charged with $50.

CAUTION: Keep in mind that most Google Cloud resources drain your billing account after creation even if they are not actively used. Therefore, delete Google Cloud projects you do not longer need or at least disconnect them from billing.

== Set the CAMPUS_ID

tag::campusid[]

In order to avoid naming collisions between projects and resources of students, we use our Campus ID for discriminators in names. Occurences of `${CAMPUS_ID}` in the following must be replaced with your personal lowercase campus ID. If `${CAMPUS_ID}` is part of a command line or a script you have to execute, you can the command line interpreter Bash make that variable substitution. This requires the variable `CAMPUS_ID` to be set (permanently). For convinence, execute the following script in each shell you use 

 curl -sL https://gitlab.com/rwoerzbe/public/-/raw/main/setcampusid.sh | bash -s yourcampusid

Replace `yourcampusid` with your campus ID.

Then restart your Bash or execute 

 source ~/.bashrc

in order (re-)execute `~/.bashrc`.

This has to be done for every Bash we will use, that are

. the Bash on your local machine
. the Cloud Shell 
. and for each virtual machine we login to via SSH. 

Please reassure yourself that `CAMPUS_ID` is actually set by executing

 echo ${CAMPUS_ID}

Occurences of `${CAMPUS_ID}` which are not in Bash shell commands but in, e.g., URLs have to be manually replaced with your Campus ID.

Some seem to prefer a https://www.zsh.org/[Z shell (`zsh`)] before a https://tiswww.case.edu/php/chet/bash/bashtop.html[Bourne Again Shell (`bash`)]. This is maybe due to the fact that MacOS uses `zsh` as default. That is why we append the `export CAMPUS_ID` also to a file `~/.zshrc`.

end::campusid[]

== Cheat Script

Some exercises are prone to mistakes that will require you to start over. Therefore I provide bash scripts that automate the steps you otherwise have to do manually.

These scripts are meant to be executed in the <<Cloud Shell, Google Cloud Shell>> like so

 curl -L https://gitlab.com/rwoerzbe/public/-/raw/main/XXX_setup.sh | bash -sx

where `XXX_setup.sh` stands for the respective script. The console output will be stored in a file like `XXX_setup-1685460412.log` for later review or debugging.

You can pass some command line options to the script. The available command line options are the following:
----
--campusid      Overwrites the environment variable CAMPUS_ID which is a 
                mandatory variable for all scripts. So, if neither the
                command line option is given nor the CAMPUS_ID
                environment variable is set, the scripts will exit
                with a corresponding error message.

--zone          The Google Cloud Zone, where the resources should
                be created. Defaults to europe-west4-a

--stage         The numerical stage after which the script will stop. 
                The stage mailny corresponds to the section of the respective
                exercise description. Defaults to a value that makes
                the respective script run completely.

--appname       The application name as it will be found in the first 
                segment of the Google Cloud Project ID.
                Each script sets a respective default.

--billing       The ID of the billing account to which a newly created
                Google Cloud project should be linked. Defaults to the
                first billing account with attribute open=true.
----

For example 

 curl -L https://gitlab.com/rwoerzbe/public/-/raw/main/kubernetes_setup.sh | bash -sx -- --campusid rwoerzbe --zone europe-west4-b --stage 9

creates a project like `k8s-rwoerzbe-12345` -- the suffix is a random number between `0` and `32767` -- in zone `europe-west4-b` and executes all steps up to and including <<Load-Balancing,Section x.9: Frontend Service>> and stores the output in a file like `kubernetes_setup-1685460412.log`.

CAUTION: Note the necessary delimiter `--` 

WARNING: Every execution of this script creates a new Google Cloud project with resources in it that will be billed. So unlink and delete the projects if you do not need them anymore. Furthermore, the maximum amount of projects that can be linked to a specific billing account is very limited even if these projects are being quickly unlinked or deleted. So, do not execute the cheat scripts just for fun.