#!/bin/bash

VALID_ARGS=$(getopt -o c:z:s:a:b:o --long campusid:,zone:,stage:,appname:,billing:,omit-log -- "$@")
if [[ $? -ne 0 ]]; then
    echo "Available command line options are:
--campusid      Overwrites the environment variable CAMPUS_ID which is a 
                mandatory variable for all scripts. So, if neither the
                command line option is given nor the CAMPUS_ID
                environment variable is set, the scripts will exit
                with a corresponding error message.

--zone          The Google Cloud Zone, where the resources should
                be created. Defaults to europe-west4-a

--stage         The numerical stage after which the script will stop. 
                The stage mailny corresponds to the section of the respective
                exercise description. Defaults to a value that makes
                the respective script run completely.

--appname       The application name as it will be found in the first 
                segment of the Google Cloud Project ID.
                Each script sets a respective default.

--billing       The ID of the billing account to which a newly created
                Google Cloud project should be linked. Defaults to the
                first billing account with attribute open=true.

--omit-log  Suppresses the script output to a file appname_setup-timestamp.log"
    exit 1;
fi

eval set -- "$VALID_ARGS"
while [ : ]; do
  case "$1" in
    -c | --campusid)
        CAMPUS_ID=$2
        shift 2
        ;;
    -z | --zone)
        ZONE=$2
        shift 2
        ;;
    -s | --stage)
        STAGE=$2
        shift 2
        ;;
    -a | --appname)
        APPNAME=$2
        shift 2
        ;;
    -b | --billing)
        BILLING=$2
        shift 2
        ;;
    -o | --omit-log)
        OMITLOG=true
        shift 1
        ;;
    --) shift; 
        break 
        ;;
  esac
done

if [[ -z "${CAMPUS_ID}" ]]; then
    echo "CAMPUS_ID is not set"
    exit 1;
fi

if [[ -z "${APPNAME}" ]]; then
    echo "APPNAME is not set"
    exit 1;
fi

# tee the output

if [[ -z "${OMITLOG}" ]]; then
    exec > >(tee -a ${APPNAME}_setup-$(date +%s).log) 2>&1
fi

if [[ -z "${ZONE}" ]]; then
    ZONE="europe-west4-a";
fi

REGION=${ZONE::-2}

if [[ -z "${STAGE}" ]]; then
    STAGE=9999;
fi

if [[ -z "${BILLING}" ]]; then
    BILLING=$(gcloud beta billing accounts list --filter='open:true' --format='value(ACCOUNT_ID)' | head -n 1);
fi

PROJECT_ID_SUFFIX=$RANDOM
DEVSHELL_PROJECT_ID=${APPNAME}-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
HLCOLOR="\033[1;32m"
ERRCOLOR="\033[0;31m"
NOCOLOR="\033[0m"

echo "We go with the following settings:
CAMPUS_ID=$CAMPUS_ID
APPNAME=$APPNAME
ZONE=$ZONE
REGION=$REGION
STAGE=$STAGE
DEVSHELL_PROJECT_ID=$DEVSHELL_PROJECT_ID
BILLING=$BILLING"