-- initial example schema and data
create table todos (
  id serial primary key,
  done boolean not null default false,
  task text not null,
  due timestamptz
);

insert into todos (task) values ('finish tutorial 0'), ('pat self on back');

-- cribed from https://postgrest.org/en/v12/references/api/resource_embedding.html
create table actors(
  id int primary key generated always as identity,
  first_name text,
  last_name text
);

create table directors(
  id int primary key generated always as identity,
  first_name text,
  last_name text
);

create table films(
  id int primary key generated always as identity,
  director_id int references directors(id),
  title text,
  year int,
  rating numeric(3,1),
  language text
);

create table technical_specs(
  film_id INT REFERENCES films UNIQUE,
  runtime TIME,
  camera TEXT,
  sound TEXT
);

create table roles(
  film_id int references films(id),
  actor_id int references actors(id),
  character text,
  primary key(film_id, actor_id)
);

create table competitions(
  id int primary key generated always as identity,
  name text,
  year int
);

create table nominations(
  competition_id int references competitions(id),
  film_id int references films(id),
  rank int,
  primary key (competition_id, film_id)
);

-- Insert sample data into actors table
INSERT INTO actors (first_name, last_name) VALUES
('Leonardo', 'DiCaprio'),
('Kate', 'Winslet'),
('Brad', 'Pitt'),
('Angelina', 'Jolie'),
('Morgan', 'Freeman');

-- Insert sample data into directors table
INSERT INTO directors (first_name, last_name) VALUES
('Steven', 'Spielberg'),
('Christopher', 'Nolan'),
('Martin', 'Scorsese'),
('James', 'Cameron'),
('Quentin', 'Tarantino');

-- Insert sample data into films table
INSERT INTO films (director_id, title, year, rating, language) VALUES
(1, 'Jurassic Park', 1993, 8.1, 'English'),
(2, 'Inception', 2010, 8.8, 'English'),
(3, 'The Wolf of Wall Street', 2013, 8.2, 'English'),
(4, 'Titanic', 1997, 7.8, 'English'),
(5, 'Pulp Fiction', 1994, 8.9, 'English');

-- Insert sample data into technical_specs table
INSERT INTO technical_specs (film_id, runtime, camera, sound) VALUES
(1, '02:07:00', 'Panavision Panaflex', 'Dolby Digital'),
(2, '02:28:00', 'ARRIFLEX 235', 'Dolby Digital'),
(3, '02:59:00', 'Red Epic', 'Dolby Atmos'),
(4, '03:14:00', 'Panavision Panaflex', 'Dolby Digital'),
(5, '02:34:00', 'Panavision Panaflex', 'Dolby Digital');

-- Insert sample data into roles table
INSERT INTO roles (film_id, actor_id, character) VALUES
(1, 5, 'Dr. Ian Malcolm'),
(2, 1, 'Dom Cobb'),
(3, 1, 'Jordan Belfort'),
(3, 3, 'Donnie Azoff'),
(4, 1, 'Jack Dawson'),
(4, 2, 'Rose DeWitt Bukater'),
(5, 3, 'Vincent Vega'),
(5, 4, 'Mia Wallace');

-- Insert sample data into competitions table
INSERT INTO competitions (name, year) VALUES
('Academy Awards', 2023),
('Cannes Film Festival', 2023),
('Golden Globe Awards', 2023),
('BAFTA Awards', 2023),
('Sundance Film Festival', 2023);

-- Insert sample data into nominations table
INSERT INTO nominations (competition_id, film_id, rank) VALUES
(1, 2, 1),
(1, 3, 2),
(2, 1, 1),
(2, 5, 2),
(3, 4, 1),
(3, 3, 2),
(4, 2, 1),
(5, 5, 1);


-- trigger schema cache reload in PostgREST on schema changes
CREATE OR REPLACE FUNCTION pgrst_watch() RETURNS event_trigger
  LANGUAGE plpgsql
  AS $$
BEGIN
  NOTIFY pgrst, 'reload schema';
END;
$$;
CREATE EVENT TRIGGER pgrst_watch
  ON ddl_command_end
  EXECUTE PROCEDURE pgrst_watch();