ifndef::imagesdir[:imagesdir: ./media]

== Design

=== Lab 0 Homework in UML

This exercise is about expressing the Lab 0 Homework in UML. Feel free to use any UML drawing tool, may it be your pen or https://plantuml.com[PlantUML].

==== UML Use Case diagram

Create a UML use case diagram for the functional requirements of the Lab 0 Homework. This should be independent the tech stack you have chose for implementation.

include::design_solutions_en.asciidoc[tag=sol-usecase]

==== UML Activity Diagram

Create a UML activity diagram that refines one or more use cases you have declared in your UML use case diagram.

include::design_solutions_en.asciidoc[tag=sol-activity]

==== UML Component Diagram (Level 0)

Create a context diagram (level 0) as a UML component diagram that models you PollApp and its neighbouring systems.

include::design_solutions_en.asciidoc[tag=sol-context]

==== UML Component Diagram (Level 1)

Create a UML component diagram that models the coarse grained structure of your PollApp code base.

include::design_solutions_en.asciidoc[tag=sol-component]

==== UML Class Diagram

Create a UML class diagram that models the database schema of your PollApp implementation.

include::design_solutions_en.asciidoc[tag=sol-class]

==== UML Deployment Diagram

Create a UML deployment diagram that models how your PollApp would be deployed in a production environment. Particular include the runtime environments that are necessary in your tech stack.

include::design_solutions_en.asciidoc[tag=sol-deployment]

==== UML Sequence Diagram

Model the control flow of the "Create Vote" on the granularity of function calls in a UML Sequence Diagram

include::design_solutions_en.asciidoc[tag=sol-sequence]

=== Information Hiding in Programming Languages

Different programming languages provide different language constructs like packages, namespaces, visibility modifiers, export modifiers etc. which help to express architectural itentions in code. For example in Java, the visibility modifier `private` prohibits references to the respective program unit (inner class, method, or member variable) from other namespaces. By using such a modifier, an architect or programmer can "hide" certain implementation details. He or she can express that he or she would consider an incoming dependency a private program unit an undesireable strengthening of coupling and connascence. 

IMPORTANT: Take two programming language you are familiar with (Java, Python, Ruby, C#, JavaScript, Typescript, Swift etc.). Compare both programming languages with regard to their information hiding capabilities. Are there situations that can just be expressed in one programming language but not the other?

include::design_solutions_en.asciidoc[tag=sol-information-hiding]

=== Decomposition Strategies and Information Hiding Constructs

Imagine a very simple program that allows for, e.g., maintaining Todo lists for multiple users. Implement such a program in a monolithic architecture. In the lecture, we have learned that you can decompose such a programm by "technical aspects" (UI, logic, persistence) or by "functionality" (Todo list management, user management).

Draft such a program either in real code in a real programming language or as a UML class diagram (but with hindsight to a real programming language). Which decomposition strategy allows for better information hiding in that particular programming language?

include::design_solutions_en.asciidoc[tag=sol-decomposition-strategies]

=== Identify Architectural Styles

Take your most favourite web framework like https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#mvc[Spring MVC] and an open source system, which uses this framework. Of course, this could be a demonstrator project like https://github.com/spring-projects/spring-petclinic[Spring Pet Clinic].

Identify the 

. decomposition pattern (by technical aspects or by functionality) and
. the architectural style (layered, hexagonal, pipes-and-filters, microkernel)

used in that system.

include::design_solutions_en.asciidoc[tag=sol-decomposition-style-petclinic]

To what extend is the underlying framework "opinionated" towards a certain decomponsition pattern or architectural style?

include::design_solutions_en.asciidoc[tag=sol-spring-opinionated]

=== Programming Models for Concurrency

Blocking waits in a program can occur due to network and I/O operations. Different programming languages provide different means for concurrency to keep a program as a whole responsive during a blocking wait.

==== JavaScript

JavaScript is single threaded. Use https://www.jsv9000.app/ to get a clue how JavaScript handles blocking waits.

==== Java

Java is multithreaded. There are https://docs.oracle.com/javase/tutorial/essential/concurrency/index.html[diverse patterns] how to manage threads in the code they execute in Java. 

IMPORTANT: https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/Executors.html#newFixedThreadPool(int)[`java.concurrency.Executors#newFixedThreadPool(int)`] allows for setting a specific number of (worker) threads. Which number should be taken into account when setting the number of threads for high utilization?

include::design_solutions_en.asciidoc[tag=sol-number-threads]
