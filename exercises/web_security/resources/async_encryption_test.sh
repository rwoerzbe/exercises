#!/bin/bash

# Variables
FILE="iliad.txt"
CHUNK_SIZE=470  # Adjusted for a 4096-bit RSA key with OAEP padding
PUBLIC_KEY="async_encryption_test_public_key.pem"
PRIVATE_KEY="async_encryption_test_private_key.pem"
TMP_DIR="/tmp/openssl_chunks"
PASSPHRASE="yourPassphrase"

# Step 1: Generate RSA keys
generate_keys() {
    openssl genpkey -algorithm RSA -out $PRIVATE_KEY -aes256 -pass pass:$PASSPHRASE -pkeyopt rsa_keygen_bits:4096
    openssl rsa -in $PRIVATE_KEY -passin pass:$PASSPHRASE -pubout -out $PUBLIC_KEY
}

# Step 2: Create temporary directory for chunks
create_tmp_dir() {
    mkdir -p $TMP_DIR
}

# Step 3: Split the file into chunks
split_file() {
    split -b $CHUNK_SIZE "$FILE" "${TMP_DIR}/${FILE}.chunk"
}

# Step 4: Encrypt each chunk with the public key
encrypt_chunks() {
    for chunk in ${TMP_DIR}/${FILE}.chunk*; do
        openssl pkeyutl -encrypt -inkey $PUBLIC_KEY -pubin -pkeyopt rsa_padding_mode:oaep -in "$chunk" -out "${chunk}.enc"
    done
}

# Step 5: Concatenate encrypted chunks
concatenate_chunks() {
    cat ${TMP_DIR}/${FILE}.chunk*.enc > "${FILE}.enc"
}

# Step 6: Decrypt each chunk with the private key
decrypt_chunks() {
    split -b 512 "${FILE}.enc" "${TMP_DIR}/${FILE}.chunk_enc_"
    for chunk in ${TMP_DIR}/${FILE}.chunk_enc_*; do
        openssl pkeyutl -decrypt -inkey $PRIVATE_KEY -passin pass:$PASSPHRASE -pkeyopt rsa_padding_mode:oaep -in "$chunk" -out "${chunk}.dec"
    done
}

# Step 7: Reassemble the decrypted chunks
reassemble_file() {
    cat ${TMP_DIR}/${FILE}.chunk_enc_*.dec > "${FILE}.decrypted"
}

# Step 8: Clean up temporary directory and keys
cleanup() {
    rm -rf $TMP_DIR
    rm -f $PUBLIC_KEY $PRIVATE_KEY
}

# Main execution
generate_keys
create_tmp_dir
split_file
encrypt_chunks
concatenate_chunks
decrypt_chunks
reassemble_file
cleanup

echo "Encryption and decryption complete. Decrypted file: ${FILE}.decrypted"

# Clean up local chunk files if any
rm -f ${FILE}.chunk*
rm -f ${FILE}.chunk*.enc
rm -f ${FILE}.chunk_enc_*.dec
