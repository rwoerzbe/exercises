#!/bin/bash
#export PGPASSWORD=postgres
docker exec -it -e PGPASSWORD=${PASSWORD:-4gH57s8X99} databases-postgres-1 bash -c '
echo -e "\e[21msetup: CREATE TABLE employees and add an employee John Doe \e[24m"
psql -U postgres -f /scripts/anomalies/non_repeatable_read/non_repeatable_read_setup.sql
wait
echo -e "\e[21mSELECT and UPDATE table employees in parallel\e[24m"
psql -U postgres -f /scripts/anomalies/non_repeatable_read/non_repeatable_read_select.sql & 
psql -U postgres -f /scripts/anomalies/non_repeatable_read/non_repeatable_read_update.sql &
wait
echo -e "\e[21mDROP TABLE employees\e[24m"
psql -U postgres -f /scripts/anomalies/non_repeatable_read/non_repeatable_read_teardown.sql
echo -e "\e[21mDone\e[24m"'