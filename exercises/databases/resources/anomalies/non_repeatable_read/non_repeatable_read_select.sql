-- transaction
BEGIN TRANSACTION ISOLATION LEVEL READ COMMITTED; -- choose isolation level here: "READ COMMITED", "REPEATABLE READ", "SERIALIZABLE"
SELECT salary FROM employees WHERE id = 1;
SELECT COUNT(*) FROM employees WHERE salary > 5000;
SELECT pg_sleep(5); -- meanwhile we update table employee in another parallel transaction
SELECT salary FROM employees WHERE id = 1; 
SELECT COUNT(*) FROM employees WHERE salary > 5000;
COMMIT;