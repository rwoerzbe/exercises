CREATE TABLE IF NOT EXISTS employees (
    id serial PRIMARY KEY,
    name varchar(100) NOT NULL,
    salary numeric(10, 2) NOT NULL
);
INSERT INTO employees (id, name, salary) VALUES (1, 'John Doe', 2000);