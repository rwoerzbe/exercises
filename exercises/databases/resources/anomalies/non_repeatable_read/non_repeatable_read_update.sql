BEGIN TRANSACTION;
SELECT pg_sleep(2); -- wait some time for the first transaction
UPDATE employees SET salary = 3000 WHERE id = 1;
INSERT INTO employees(id, name, salary) VALUES (2, 'Bernd Boss', 8000);
COMMIT;