#!/bin/bash

# Database connection details for PgBouncer
export PGUSER="myuser"
export PGPASSWORD="mypassword"
PGDATABASE="postgres"
PGHOST="localhost"
PGPORT="6432"

# Function to perform a write operation (increment the counter)
write_operation() {
  psql -U $PGUSER -d $PGDATABASE -h $PGHOST -p $PGPORT -c "UPDATE counter_table SET counter = counter + 1;"
}

# Function to perform a read operation (get the current value of the counter)
read_operation() {
  psql -U $PGUSER -d $PGDATABASE -h $PGHOST -p $PGPORT -c "SELECT counter FROM counter_table;" -t -A
}

# Create table if not exists on master
psql -U $PGUSER -d $PGDATABASE -h $PGHOST -p $PGPORT -c "CREATE TABLE IF NOT EXISTS counter_table (counter INT);"

# Initialize the counter if it doesn't exist
psql -U $PGUSER -d $PGDATABASE -h $PGHOST -p $PGPORT -c "INSERT INTO counter_table (counter) SELECT 0 WHERE NOT EXISTS (SELECT 1 FROM counter_table);"

# Loop to perform 100 write and read operations
for ((i = 1; i <= 1000; i++)); do
  #echo "Performing write operation $i..."
  write_operation
  current_value=$(read_operation)
  echo "Current counter value: $current_value"
done

echo "Completed all operations."
