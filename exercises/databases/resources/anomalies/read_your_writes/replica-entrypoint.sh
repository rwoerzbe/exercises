#!/bin/bash

# Ensure the data directory is owned by the postgres user and has correct permissions
chown -R postgres:postgres /var/lib/postgresql/data
chmod 700 /var/lib/postgresql/data

# Wait for the master to be ready
until pg_isready -h postgres-master -p 5432; do
  sleep 1
done

# Run the base backup to initialize the replica, only if the data directory is empty
if [ -z "$(ls -A /var/lib/postgresql/data)" ]; then
  su postgres -c "
    until pg_basebackup -h postgres-master -D /var/lib/postgresql/data -U myuser -v -P -R
    do
    echo 'Waiting for primary to connect...'
    sleep 1s
    done
  "
  # Configure the primary_conninfo
  echo "primary_conninfo = 'host=postgres-master port=5432 user=myuser password=mypassword'" >> /var/lib/postgresql/data/postgresql.auto.conf
fi

# Start PostgreSQL
exec su postgres -c "postgres -D /var/lib/postgresql/data -c config_file=/etc/postgresql/postgresql.conf -c hba_file=/etc/postgresql/pg_hba.conf"
