#!/bin/bash
#export PGPASSWORD=postgres
docker exec -it -e PGPASSWORD=${PASSWORD:-4gH57s8X99} databases-postgres-1 bash -c '
echo -e "\e[21msetup: CREATE TABLE keyvalue and INSERT x and y\e[24m"
psql -U postgres -f /scripts/anomalies/write_skew/write_skew_setup.sql
wait
echo -e "\e[21mExecuting both UPDATE transactions dec_y and inc_x in parallel\e[24m"
psql -U postgres -f /scripts/anomalies/write_skew/write_skew_dec_y.pgsql &
psql -U postgres -f /scripts/anomalies/write_skew/write_skew_inc_x.pgsql &
wait
echo -e "\e[21mRedo inc_x\e[24m"
psql -U postgres -f /scripts/anomalies/write_skew/write_skew_inc_x.pgsql
echo -e "\e[21mFinal SELECT of keyvalue table + DROP TABLE keyvalue\e[24m"
psql -U postgres -f /scripts/anomalies/write_skew/write_skew_teardown.sql
echo -e "\e[21mDone\e[24m"'