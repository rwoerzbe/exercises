BEGIN;
  SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;
  DO $$
  DECLARE
    x integer;
    y integer;
  BEGIN
    SELECT value INTO x FROM keyvalue WHERE key = 'x';
    SELECT value INTO y FROM keyvalue WHERE key = 'y';
    IF x+1 < y THEN
      PERFORM pg_sleep(1);
      UPDATE keyvalue SET value = value + 1 WHERE key = 'x';
    ELSE 
      RAISE NOTICE 'No update. Constraint x < y would be violated';
    END IF;
  END;
  $$ LANGUAGE plpgsql;
COMMIT;