CREATE TABLE IF NOT EXISTS keyvalue (
    key varchar(100) PRIMARY KEY,
    value integer NOT NULL
);

INSERT INTO keyvalue (key, value) VALUES ('x', 1);
INSERT INTO keyvalue (key, value) VALUES ('y', 3);