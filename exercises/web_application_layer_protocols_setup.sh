#!/bin/bash

APPNAME="walp"

source <(curl -s https://gitlab.com/rwoerzbe/public/-/raw/main/generic_setup.sh)

if [[ -z "${PASSWORD}" ]]; then
   PASSWORD=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 30 ; echo '');
fi

if [ "${STAGE}" -ge 1 ]
then
gcloud projects create ${APPNAME}-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud config set project ${APPNAME}-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud beta billing projects link ${DEVSHELL_PROJECT_ID} --billing-account=${BILLING}
gcloud services enable compute.googleapis.com
gcloud config set compute/region ${REGION}
gcloud config set compute/zone ${ZONE}
gcloud compute instances create vminstance-01 \
--image-project=ubuntu-os-cloud \
--image-family=ubuntu-2004-lts \
--tags=http-server \
--metadata=startup-script='#! /bin/bash
sudo apt update -y
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update -y
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
sudo touch /startup-script-finished'

while [ "$(gcloud compute ssh vminstance-01 --command 'ls /startup-script-finished')" != "/startup-script-finished" ]
do
    echo Startup script still in progres. Stay patient!
    sleep 10
done
gcloud compute ssh vminstance-01 --command "sudo usermod -aG docker ${USER}"
gcloud compute firewall-rules create openapi --allow tcp:3000
gcloud compute firewall-rules create postgraphile --allow tcp:5000
gcloud compute firewall-rules create swaggereditor --allow tcp:8081
gcloud compute firewall-rules create pgadmin --allow tcp:8082
gcloud compute ssh vminstance-01 --command "curl -sSL https://gitlab.com/rwoerzbe/public/-/raw/main/web_application_layer_protocols.tar.gz | tar xz"
HOST=$(gcloud compute instances describe vminstance-01 --format='get(networkInterfaces[0].accessConfigs[0].natIP)')
gcloud compute ssh vminstance-01 --command "HOST=${HOST} PASSWORD=${PASSWORD} docker compose up -d"
echo "
Raw OpenAPI: http://${HOST}:3000 (try, e.g., a GET on http://${HOST}:3000/todos )
Swagger Editor: http://${HOST}:8081
GraphQL endpoint: http://${HOST}:5000/graphql
PostGraphiQL Editor: http://${HOST}:5000/graphiql
PgAdmin: http://${HOST}:8082 (login with user@example.com / ${PASSWORD} . Same password for the connection to 'database')"
fi

