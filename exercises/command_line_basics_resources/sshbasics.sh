#!/bin/bash

# Jeder der folgenden Kommentare fordert Sie auf, genau eine Kommandozeile als Lösung zu formulieren.
# Mitunter müssen hierbei jedoch mehrere Kommandos mittels Pipe verkettet werden.
# Redirections in oder aus Dateien sowie Expansionen sind mitunter auch zur Lösung nötig.
# Lösen Sie die Aufgaben in einer Bash-Shell. 
# Kopieren Sie die jeweilige Lösung unterhalb des jeweiligen Kommentars.

# 1. Auf der vminstance-01: Login für bestimmen User auf vminstance-01 erlauben
# Ermitteln Sie Ihren Username

# 2. Auf der vminstance-01: Passwortbasierten Login für Ihren User erlauben
# Fügen Sie in der vminstance-01 in der Datei /etc/ssh/sshd_config die Zeile
#   AllowUsers IhrUsername
# ein und ändern Sie die Zeile
#   PasswordAuthentication no
# in 
#   PasswordAuthentication yes

# 3. Auf der vminstance-01: Starten Sie die den Dienst (Daemon) sshd mit dem Kommando systemctl neu, 
# damit die geänderte Konfigurationsdatei wirksam wird

# 4. Auf der vminstance-02: Erzeugen Sie mit dem Kommando ssh-keygen auf der vminstance-02 ein Schlüsselpaar.

# 5. Auf der vminstance-01: Ermitteln Sie auf der vminstance-01 mittels
# hostname -f
# oder
# curl "http://metadata.google.internal/computeMetadata/v1/instance/hostname"  -H "Metadata-Flavor: Google"
# den vollqualifizierten Domain-Name der vminstance-01

# 6. Auf der vminstance-02: Fügen Sie die vminstance-01 mittels ssh-keyscan in ~/.ssh/known_hosts der vminstance-02 ein

# 7. Auf der vminstance-01: Legen Sie ein Passwort für den aktuellen User fest
# Verwenden Sie hierfür sudo passwd

# 8. Auf der vminstance-02: Public Key kopieren von vminstance-02 nach vminstance-01
# Fügen Sie den Inhalt von ~/.ssh/id_rsa.pub auf der vminstance-02 in ~/.ssh/authorized_keys ein.
# Benutzen Sie hierfür ssh-copy-id, der das automatisiert

# 9. Auf der vminstance-02: Loggen Sie sich per SSH auf der vminstance-01 ein

