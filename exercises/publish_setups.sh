#!/bin/bash
DOWNLOADDIR=../../../downloads

cp *_setup.sh $DOWNLOADDIR
cp command_line_basics_resources/clbasics.sh $DOWNLOADDIR
cp web_security/resources/async_encryption_test.sh $DOWNLOADDIR
tar cfz $DOWNLOADDIR/databases_resources.tar.gz -C databases/resources .
tar cfz $DOWNLOADDIR/web_application_layer_protocols.tar.gz -C web_application_layer_protocols/resources .

git -C $DOWNLOADDIR add databases_resources.tar.gz web_application_layer_protocols.tar.gz *_setup.sh clbasics.sh
git -C $DOWNLOADDIR commit -m "Modified *_setup.sh"
git -C $DOWNLOADDIR push