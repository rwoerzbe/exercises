#!/bin/bash

APPNAME="docker"

source <(curl -s https://gitlab.com/rwoerzbe/public/-/raw/main/generic_setup.sh)

if [ "${STAGE}" -ge 1 ]
then
gcloud projects create ${APPNAME}-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud config set project ${APPNAME}-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud beta billing projects link ${DEVSHELL_PROJECT_ID} --billing-account=${BILLING}
gcloud services enable compute.googleapis.com
gcloud config set compute/region ${REGION}
gcloud config set compute/zone ${ZONE}
gcloud compute instances create vminstance-01 \
--image-project=ubuntu-os-cloud \
--image-family=ubuntu-2004-lts \
--tags=http-server \
--metadata=startup-script='#! /bin/bash
sudo apt update -y
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update -y
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
sudo touch /startup-script-finished'

while [ "$(gcloud compute ssh vminstance-01 --command 'ls /startup-script-finished')" != "/startup-script-finished" ]
do
    echo Startup script still in progres. Stay patient!
    sleep 10
done
gcloud compute ssh vminstance-01 --command "sudo usermod -aG docker ${USER}"
fi

if [ "${STAGE}" -ge 2 ]
then
gcloud compute instances describe vminstance-01 --format='get(networkInterfaces[0].accessConfigs[0].natIP)'

echo "<html><body>My own webpage</body></html>" >> mypage.html
cat <<EOT > Dockerfile
FROM jgcl88/alpine-nginx-php:ci-69
COPY ./mypage.html /app/public
EOT

docker image build --tag mysite-nginx:latest .
docker image tag mysite-nginx:latest europe-west4-docker.pkg.dev/${DEVSHELL_PROJECT_ID}/docker-repo/mysite-nginx:1.0
gcloud services enable artifactregistry.googleapis.com
gcloud auth configure-docker europe-west4-docker.pkg.dev
gcloud artifacts repositories create docker-repo --repository-format=docker --location=europe-west4
docker image push europe-west4-docker.pkg.dev/${DEVSHELL_PROJECT_ID}/docker-repo/mysite-nginx:1.0
gcloud compute ssh vminstance-01 --command "echo Y | gcloud auth configure-docker europe-west4-docker.pkg.dev"
gcloud compute ssh vminstance-01 --command "docker image push europe-west4-docker.pkg.dev/${DEVSHELL_PROJECT_ID}/docker-repo/mysite-nginx:1.0"
gcloud compute ssh vminstance-01 --command "docker run --detach --name mysite-nginx europe-west4-docker.pkg.dev/${DEVSHELL_PROJECT_ID}/docker-repo/mysite-nginx:1.0"
fi