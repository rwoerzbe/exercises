#!/bin/bash

APPNAME="mas"

source <(curl -s https://gitlab.com/rwoerzbe/public/-/raw/main/generic_setup.sh)

if [ "${STAGE}" -ge 1 ]
then
gcloud projects create ${DEVSHELL_PROJECT_ID}
gcloud config set project ${DEVSHELL_PROJECT_ID}
gcloud beta billing projects link ${DEVSHELL_PROJECT_ID} --billing-account=${BILLING}
gcloud services enable compute.googleapis.com
gcloud config set compute/region ${REGION}
gcloud config set compute/zone ${ZONE}
gcloud compute instances create vminstance-01 \
--image-project=ubuntu-os-cloud \
--image-family=ubuntu-2004-lts \
--metadata=startup-script='#! /bin/bash
sudo apt update -y
sudo apt install openjdk-17-jdk-headless -y
sudo touch /startup-script-finished'

while [ "$(gcloud compute ssh vminstance-01 --command 'ls /startup-script-finished')" != "/startup-script-finished" ]
do
    echo Startup script still in progres. Stay patient!
    sleep 10
done

gcloud compute ssh vminstance-01 --command "curl -s -L https://dlcdn.apache.org/kafka/3.7.0/kafka_2.13-3.7.0.tgz | tar xvfz - -C ."
gcloud compute ssh vminstance-01 --command "nohup kafka_2.13-3.7.0/bin/zookeeper-server-start.sh kafka_2.13-3.7.0/config/zookeeper.properties 1>zookeeper.stout 2>zookeeper.sterr &"
sleep 30 
gcloud compute ssh vminstance-01 --command "nohup kafka_2.13-3.7.0/bin/kafka-server-start.sh kafka_2.13-3.7.0/config/server.properties 1>kafka.stout 2>kafka.stderr &"
sleep 30

# needs to run seperately, otherwise the command above does not return to the Cloud Shell 
gcloud compute ssh vminstance-01 --command "kafka_2.13-3.7.0/bin/kafka-topics.sh --create --topic quickstart-events --bootstrap-server localhost:9092"

echo -e "${HLCOLOR} Open a SSH shell on vminstance-01 and execute
kafka_2.13-3.7.0/bin/kafka-console-producer.sh --topic quickstart-events --bootstrap-server localhost:9092
Open another SSH shell on vminstance-01 and execute
kafka_2.13-3.7.0/bin/kafka-console-consumer.sh --topic quickstart-events --from-beginning --bootstrap-server localhost:9092
Then type in some messages in the first shell (delimited with Return) and see them being printed in the second shell
You can also stop the receiver inbetween with Ctrl+C and still produce messages. If you restart the receiver, it will fetch the messages on start.${NOCOLOR}"
fi

if [ "${STAGE}" -ge 2 ]
then
gcloud services enable pubsub.googleapis.com
gcloud pubsub topics create exampleTopic
gcloud pubsub subscriptions create exampleSubscription --topic=exampleTopic
export JAVA_HOME=/usr/lib/jvm/java-1.17.0-openjdk-amd64

rm -rf spring-integration-sender
curl https://start.spring.io/starter.tgz \
  -d bootVersion=3.0.5 \
  -d dependencies=web,integration,cloud-gcp-pubsub \
  -d type=maven-project \
  -d baseDir=spring-integration-sender | tar -xzvf -


echo 'package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.integration.annotation.MessagingGateway;
import com.google.cloud.spring.pubsub.core.PubSubTemplate;
import com.google.cloud.spring.pubsub.integration.outbound.PubSubMessageHandler;

import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.MessageHandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

@SpringBootApplication
@RestController
public class DemoApplication {

    @Autowired
    private PubsubOutboundGateway messagingGateway;

    @PostMapping("/postMessage")
    public RedirectView postMessage(@RequestParam("message") String message) {
        this.messagingGateway.sendToPubsub(message);
        return new RedirectView("/");
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @MessagingGateway(defaultRequestChannel = "pubsubOutputChannel")
    public interface PubsubOutboundGateway {
        void sendToPubsub(String text);
    }

    @Bean
    @ServiceActivator(inputChannel = "pubsubOutputChannel")
    public MessageHandler messageSender(PubSubTemplate pubsubTemplate) {
        return new PubSubMessageHandler(pubsubTemplate, "exampleTopic");
    }

}' > spring-integration-sender/src/main/java/com/example/demo/DemoApplication.java

export GOOGLE_CLOUD_PROJECT=`gcloud config list --format 'value(core.project)'`
cd spring-integration-sender ; ./mvnw spring-boot:run > sender_output.log &
cd ~

sleep 60

rm -rf spring-integration-receiver
curl https://start.spring.io/starter.tgz \
  -d bootVersion=3.0.5 \
  -d dependencies=web,integration,cloud-gcp-pubsub \
  -d type=maven-project \
  -d baseDir=spring-integration-receiver | tar -xzvf -

echo 'package com.example.demo;

import org.springframework.boot.SpringApplication;
import com.google.cloud.spring.pubsub.core.PubSubTemplate;
import com.google.cloud.spring.pubsub.integration.inbound.PubSubInboundChannelAdapter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;

@SpringBootApplication
public class DemoApplication {

    private static final Log LOGGER = LogFactory.getLog(DemoApplication.class);

    @ServiceActivator(inputChannel = "pubsubInputChannel")
    public void messageReceiver(String payload) {
        LOGGER.info("Message arrived! Payload: " + payload);
    }

    @Bean
    public MessageChannel pubsubInputChannel() {
        return new DirectChannel();
    }

    @Bean
    public PubSubInboundChannelAdapter messageChannelAdapter(
            @Qualifier("pubsubInputChannel") MessageChannel inputChannel, PubSubTemplate pubSubTemplate) {
        PubSubInboundChannelAdapter adapter = new PubSubInboundChannelAdapter(pubSubTemplate, "exampleSubscription");
        adapter.setOutputChannel(inputChannel);

        return adapter;
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}' > spring-integration-receiver/src/main/java/com/example/demo/DemoApplication.java

cd spring-integration-receiver ; ./mvnw spring-boot:run -Dspring-boot.run.jvmArguments="-Dserver.port=8081" > receiver_output.log &

echo -e "${HLCOLOR}Post string 'Hello world!' to spring-integration-sender${NOCOLOR}"
curl --data "message=Hello world!" localhost:8080/postMessage
sleep 10
if [[ $(tail -n 1 ~/spring-integration-receiver/receiver_output.log) == *"Hello world"* ]]
then
    echo -e "${HLCOLOR} SUCCESS: Message has successfully been delivered to receiver. Use tail -f ~/spring-integration-receiver/receiver_output.log to output the received messages.${NOCOLOR}"
else 
    echo -e "${ERRCOLOR} ERROR: Message has has not been delivered to receiver${NOCOLOR}"
fi
fi

if [ "${STAGE}" -ge 3 ]
then
gcloud services enable compute.googleapis.com
gcloud services enable pubsub.googleapis.com
gcloud services enable dataflow.googleapis.com
gcloud services enable stackdriver.googleapis.com
gcloud services enable storage-component.googleapis.com
gcloud services enable storage-api.googleapis.com
gcloud services enable bigquery-json.googleapis.com
gcloud services enable datastore.googleapis.com

gsutil mb -c multi_regional gs://${DEVSHELL_PROJECT_ID}-bucket-01

cd ~
rm -rf first-dataflow
mvn archetype:generate \
    -DarchetypeGroupId=org.apache.beam \
    -DarchetypeArtifactId=beam-sdks-java-maven-archetypes-examples \
    -DarchetypeVersion=2.46.0 \
    -DgroupId=org.example \
    -DartifactId=first-dataflow \
    -Dversion="0.1" \
    -Dpackage=org.apache.beam.examples \
    -DinteractiveMode=false

cd ~/first-dataflow

mvn compile exec:java \
      -Pdataflow-runner compile exec:java \
      -Dexec.mainClass=org.apache.beam.examples.WordCount \
      -Dexec.args="--project=${DEVSHELL_PROJECT_ID} --stagingLocation=gs://${DEVSHELL_PROJECT_ID}-bucket-01/staging/ --output=gs://${DEVSHELL_PROJECT_ID}-bucket-01/output --runner=DataflowRunner --region=${REGION} --gcpTempLocation=gs://${DEVSHELL_PROJECT_ID}-bucket-01/temp"

echo -e "${HLCOLOR}A visual representation can be found under the single job at https://console.cloud.google.com/dataflow?project=${DEVSHELL_PROJECT_ID}${NOCOLOR}"
echo -e "${HLCOLOR}The result of the pipeline run can be found at https://console.cloud.google.com/storage/browser/_details/${DEVSHELL_PROJECT_ID}-bucket-01/output-00000-of-00001;tab=live_object?authuser=0${NOCOLOR}"
fi