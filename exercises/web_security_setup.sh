#!/bin/bash

APPNAME="websec"

source <(curl -s https://gitlab.com/rwoerzbe/public/-/raw/main/generic_setup.sh)

if [[ -z "${KEYCLOAK_PASSWORD}" ]]; then
    KEYCLOAK_PASSWORD="x79bi1vzU77";
fi

if [[ -z "${KEYSTORE_PASSWORD}" ]]; then
    KEYSTORE_PASSWORD="x79bi1vzU77";
fi

if [ "${STAGE}" -ge 1 ]
then
gcloud projects create ${APPNAME}-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud config set project ${APPNAME}-${CAMPUS_ID}-${PROJECT_ID_SUFFIX}
gcloud beta billing projects link ${DEVSHELL_PROJECT_ID} --billing-account=${BILLING}
gcloud services enable compute.googleapis.com
gcloud config set compute/region ${REGION}
gcloud config set compute/zone ${ZONE}
gcloud compute firewall-rules create http --allow tcp:80
gcloud compute firewall-rules create httpdev --allow tcp:8080
gcloud compute firewall-rules create https --allow tcp:443
gcloud services enable domains.googleapis.com
gcloud services enable dns.googleapis.com
sleep 90
gcloud dns managed-zones create managedzone-01 --dns-name=${DEVSHELL_PROJECT_ID}.com --description=
cat > contacts.yaml <<EOF
allContacts:
  email: 'rene.woerzberger@th-koeln.de'
  phoneNumber: '+49.22182754324 '
  postalAddress:
    regionCode: 'DE'
    postalCode: '50679'
    locality: 'Koeln'
    addressLines: ['Betzdorfer Str. 2']
    recipients: ['Rene Woerzberger']
EOF
echo "y" | gcloud domains registrations register ${DEVSHELL_PROJECT_ID}.com --cloud-dns-zone=managedzone-01 --contact-data-from-file=contacts.yaml
for servername in resourceserver authorizationserver client
do
    gcloud compute instances create $servername-01 --image-project ubuntu-os-cloud --image-family ubuntu-2004-lts --metadata=startup-script='#! /bin/bash
    sudo apt update -y
    sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt-get update -y
    sudo apt-get install docker-ce docker-ce-cli containerd.io -y
    sudo apt install docker-compose -y
    sudo touch /startup-script-finished'
    sleep 90
    while [ "$(gcloud compute ssh $servername-01 --command 'ls /startup-script-finished')" != "/startup-script-finished" ]
    do
        echo Startup script still in progres. Stay patient!
        sleep 10
    done
    gcloud compute ssh $servername-01 --command "sudo usermod -aG docker ${USER}"
    gcloud compute addresses create $servername-01-address --region=${REGION} --addresses=$(gcloud compute instances describe $servername-01 --format='get(networkInterfaces[0].accessConfigs[0].natIP)')
    sudo apt-get install -y iputils-ping
    while ! ping -c 1 $(gcloud compute instances describe $servername-01 --format='get(networkInterfaces[0].accessConfigs[0].natIP)') &> /dev/null
    do
        echo IP address $(gcloud compute instances describe $servername-01 --format='get(networkInterfaces[0].accessConfigs[0].natIP)') still not pingable. Stay patient!
        sleep 10
    done
    gcloud dns record-sets transaction start --zone=managedzone-01
    gcloud dns record-sets transaction add $(gcloud compute instances describe $servername-01 --format='get(networkInterfaces[0].accessConfigs[0].natIP)') --name=$servername-01.${DEVSHELL_PROJECT_ID}.com. --ttl=7200 --type=A --zone=managedzone-01
    gcloud dns record-sets transaction execute --zone=managedzone-01
    while ! ping -c 1 $servername-01.${DEVSHELL_PROJECT_ID}.com &> /dev/null
    do
        echo Subdomain $servername-01.${DEVSHELL_PROJECT_ID}.com still not pingable. Stay patient!
        sleep 10
    done
    gcloud compute ssh $servername-01 --command "docker run -p 80:80 -p 443:443 -v /etc/letsencrypt:/etc/letsencrypt certbot/certbot:v2.6.0 certonly --standalone --agree-tos --reinstall --force-renewal --non-interactive --text --rsa-key-size 4096 --domains '$servername-01.${DEVSHELL_PROJECT_ID}.com' --email ${CAMPUS_ID}@smail.th-koeln.de -v"
    sleep 30
    while [ $(gcloud compute ssh $servername-01 --command "sudo ls /etc/letsencrypt/live/$servername-01.${DEVSHELL_PROJECT_ID}.com/fullchain.pem") != "/etc/letsencrypt/live/$servername-01.${DEVSHELL_PROJECT_ID}.com/fullchain.pem" ]
    do
        echo Letsencrypt still in progres. Stay patient!
        sleep 10
    done
done
fi

if [ "${STAGE}" -ge 2 ]
then
    gcloud compute ssh authorizationserver-01 --command "sudo cp /etc/letsencrypt/live/authorizationserver-01.${DEVSHELL_PROJECT_ID}.com/privkey.pem /etc/letsencrypt/live/authorizationserver-01.${DEVSHELL_PROJECT_ID}.com/tls.key ;
    sudo cp /etc/letsencrypt/live/authorizationserver-01.${DEVSHELL_PROJECT_ID}.com/fullchain.pem /etc/letsencrypt/live/authorizationserver-01.${DEVSHELL_PROJECT_ID}.com/tls.crt ;
    sudo chmod g+r /etc/letsencrypt/live/authorizationserver-01.${DEVSHELL_PROJECT_ID}.com/tls.crt ;
    sudo chmod g+r /etc/letsencrypt/live/authorizationserver-01.${DEVSHELL_PROJECT_ID}.com/tls.key ;
    docker run --detach --name keycloak --publish 443:8443 --env KEYCLOAK_USER=admin --env KEYCLOAK_PASSWORD=${KEYCLOAK_PASSWORD} --volume /etc/letsencrypt/live/authorizationserver-01.${DEVSHELL_PROJECT_ID}.com/:/etc/x509/https jboss/keycloak:16.1.1"
    sleep 120
    read -p "Create realm websec and set it up as instructed in https://gitlab.nt.fh-koeln.de/gitlab/common/exercises/-/blob/master/exercises/web_security_en.asciidoc#user-content-setup-production-authorization-server
    THEN press any key to continue..." -n1 -s 
    read -p "Now enter the parcerclient Client Secret and confirm with Return. 
Client Secret: " CLIENT_SECRET
fi

if [ "${STAGE}" -ge 3 ]
then
    for servername in resourceserver client
    do
        gcloud compute ssh $servername-01 --command "sudo -E openssl pkcs12 -export -in /etc/letsencrypt/live/$servername-01.${DEVSHELL_PROJECT_ID}.com/fullchain.pem -inkey /etc/letsencrypt/live/$servername-01.${DEVSHELL_PROJECT_ID}.com/privkey.pem -CAfile /etc/letsencrypt/live/$servername-01.${DEVSHELL_PROJECT_ID}.com/chain.pem -out /etc/letsencrypt/live/$servername-01.${DEVSHELL_PROJECT_ID}.com/keystore.p12 -password pass:${KEYSTORE_PASSWORD} -name tomcat -caname root ;
        sudo apt update -y ;
        sudo apt install openjdk-11-jdk-headless -y"
    done
    gcloud compute ssh resourceserver-01 --command "curl -sL https://gitlab.com/rwoerzbe/public/-/raw/main/parcerrs_solution.tar.gz | tar xvfz - -C ~ ; chmod +x ~/mvnw"
    gcloud compute ssh resourceserver-01 --command "sudo nohup ./mvnw clean spring-boot:run -Dspring-boot.run.profiles=prod -Dspring-boot.run.arguments=\"--spring.security.oauth2.resourceserver.jwt.issuer-uri=https://authorizationserver-01.${DEVSHELL_PROJECT_ID}.com/auth/realms/websec --server.ssl.key-store=/etc/letsencrypt/live/resourceserver-01.${DEVSHELL_PROJECT_ID}.com/keystore.p12 --server.ssl.key-store-password=${KEYSTORE_PASSWORD}\" >> nohup.log &" &
    sleep 30
    gcloud compute ssh client-01 --command "curl -sL https://gitlab.com/rwoerzbe/public/-/raw/main/parcerclient_solution.tar.gz | tar xvfz - -C ~ ; chmod +x ~/mvnw"
    gcloud compute ssh client-01 --command "sudo nohup ./mvnw clean spring-boot:run -Dspring-boot.run.profiles=prod -Dspring-boot.run.arguments=\"--spring.security.oauth2.resourceserver.jwt.issuer-uri=https://authorizationserver-01.${DEVSHELL_PROJECT_ID}.com/auth/realms/websec --server.ssl.key-store=/etc/letsencrypt/live/client-01.${DEVSHELL_PROJECT_ID}.com/keystore.p12 --server.ssl.key-store-password=${KEYSTORE_PASSWORD} --spring.security.oauth2.client.provider.keycloak.issuer-uri=https://authorizationserver-01.${DEVSHELL_PROJECT_ID}.com/auth/realms/websec --spring.security.oauth2.client.registration.keycloak.client-secret=${CLIENT_SECRET} --resourceserver.api.url=https://resourceserver-01.${DEVSHELL_PROJECT_ID}.com\" >> nohup.log &" &
    sleep 30
    read -p "Extend realm websec as instructed in https://gitlab.nt.fh-koeln.de/gitlab/common/exercises/-/blob/master/exercises/web_security_en.asciidoc#user-content-implement-new-requirement-create-customer
    THEN press any key to continue..." -n1 -s
fi